<%@ page language="java" pageEncoding="UTF-8"%>
<html>
<head>
<script src="<%=request.getContextPath()%>/static/angular/js/angular.min.js"></script>
</head>
<body>
	<h2>Hello World!</h2>
	<div ng-app="main" ng-controller="mainCtrl">
		<ul>
			<li ng-repeat="x in list">{{ x.name + ', ' + x.code }}</li>
		</ul>
	</div>
	<script>
		var app = angular.module('main', []);
		app.controller('mainCtrl', function($scope, $http) {
    		$http.get("bin/indexData").success(function(response){
    			$scope.list = response.data;
    		});
		});
	</script>
</body>
</html>