<%@ page language="java" pageEncoding="UTF-8"%>
<html>
<head>
<link href="<%=request.getContextPath()%>/static/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<script src="http://api.map.baidu.com/api?v=2.0&ak=fImBXfttcTm2lSEzxmdqhqWh" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/static/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">地图演示</a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="#">首页</a></li>
					<!-- 
					<li><a href="#about">About</a></li>
					<li><a href="#contact">Contact</a></li>
					 -->
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>

	<div class="container">
		<div id="map" style="width: 100%; height: 500px"></div>
		<div class="btn-group" role="group" aria-label="...">
			<button type="button" class="btn btn-primary" onclick="draw()">绘制区域</button>
			<button type="button" class="btn btn-primary"
				onclick="setColorOfPolygon()">设置区域颜色</button>
			<button type="button" class="btn btn-primary" onclick="getBounds()">获取区域数据</button>
			<button type="button" class="btn btn-primary" onclick="stopDraw()">停止绘制</button>
			<button type="button" class="btn btn-primary" onclick="removeDraw()">取消绘制</button>
			<!-- 
			<button type="button" class="btn btn-default">Middle</button>
			<button type="button" class="btn btn-default">Right</button>
			 -->
		</div>
	</div>
</body>
<script type="text/javascript">
	var map = new BMap.Map('map');
	var polygon;
	map.centerAndZoom(new BMap.Point(103.745, 29.600), 1);
	map.addControl(new BMap.NavigationControl());
	map.addControl(new BMap.ScaleControl());
	map.addControl(new BMap.OverviewMapControl());
	map.addControl(new BMap.MapTypeControl());
	map.setCurrentCity("宜宾市");
	map.enableScrollWheelZoom(true);

	setTimeout(function() {
		getBoundary("成都市");
	}, 1000);

	function getBoundary(addr) {
		var bdary = new BMap.Boundary();
		bdary.get(addr, function(rs) { //获取行政区域
			//map.clearOverlays();        //清除地图覆盖物       
			var count = rs.boundaries.length; //行政区域的点有多少个
			if (count === 0) {
				alert('未能获取当前输入行政区域');
				return;
			}
			var pointArray = [];
			for (var i = 0; i < count; i++) {
				var ply = new BMap.Polygon(rs.boundaries[i], {
					strokeWeight : 2,
					strokeColor : "#ff0000"
				}); //建立多边形覆盖物
				
				$.ajax({
					url : "<%=request.getContextPath()%>/map/getPath",
					method : "post",
					data :  {"pathStr":rs.boundaries[i]},
					success : function(data) {
						
					},
					error : function() {
						
					}
				});

				var opts = {
					width : 200, // 信息窗口宽度
					height : 100, // 信息窗口高度
					title : addr, // 信息窗口标题
					enableMessage : true,//设置允许信息窗发送短息
					message : addr
				}
				var infoWindow = new BMap.InfoWindow(addr, opts); // 创建信息窗口对象 

				ply.addEventListener("click", function(event) {
					map.openInfoWindow(infoWindow, event.point);
				});
				map.addOverlay(ply); //添加覆盖物
				pointArray = pointArray.concat(ply.getPath());
			}
			map.setViewport(pointArray);    //调整视野          
		});
	}

	function draw() {
		if (!polygon) {
			polygon = new BMap.Polygon([ new BMap.Point(103.745111, 29.701123),
					new BMap.Point(103.853121, 29.702234),
					new BMap.Point(103.853121, 29.603345),
					new BMap.Point(103.745111, 29.604456) ], {
				strokeColor : "blue",
				strokeWeight : 1,
				strokeOpacity : 0.5
			}); //创建多边形
		}

		polygon.setFillColor("silver");
		polygon.enableEditing();

		map.addOverlay(polygon); //增加多边形
	}

	function stopDraw() {
		if (polygon) {
			polygon.disableEditing();
		} else {
			alert("没有找到已绘制的区域");
		}
	}

	function removeDraw() {
		if (polygon) {
			map.removeOverlay(polygon);
		} else {
			alert("没有找到已绘制的区域");
		}
	}

	function setColorOfPolygon() {
		if (polygon) {
			polygon.setFillColor("#F75000");
		} else {
			alert("没有找到已绘制的区域");
		}
	}

	function getBounds() {
		if (polygon) {
			paths = polygon.getPath();
			alert(paths);
		} else {
			alert("没有找到已绘制的区域");
		}
	}
</script>
</html>