<%@ page language="java" pageEncoding="UTF-8"%>
<html>
<head>
<%
	String path = request.getContextPath();
	request.setAttribute("path", path);
%>
<link rel="stylesheet" href="${path}/static/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" href="${path}/static/jquery/plugin/loading/css/showLoading.css" />
<link rel="stylesheet" href="${path}/static/jquery/plugin/dialog/css/ui-dialog.css" />
<link rel="stylesheet" href="${path}/static/ztree/css/zTreeStyle/zTreeStyle.css" />
<link rel="stylesheet" href="${path}/static/ztree/css/demo.css" />
<script src="http://api.map.baidu.com/api?v=2.0&ak=fImBXfttcTm2lSEzxmdqhqWh" type="text/javascript"></script>
<script src="${path}/static/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="${path}/static/jquery/plugin/dialog/dist/dialog-min.js"></script>
<script src="${path}/static/jquery/plugin/dialog/dist/dialog-plus-min.js"></script>
<script src="${path}/static/core/js/app.js"></script>
<script src="${path}/static/jquery/plugin/loading/js/jquery.showLoading.min.js"></script>
<script src="${path}/static/ztree/js/jquery.ztree.all-3.5.js"></script>

<!--加载鼠标绘制工具-->
<script type="text/javascript" src="http://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.js"></script>
<link rel="stylesheet" href="http://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.css" />

</head>
<body id="body">
	<nav class="navbar navbar-default navbar-static-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">地图演示</a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="#">首页</a></li>
					<!-- 
					<li><a href="#about">About</a></li>
					<li><a href="#contact">Contact</a></li>
					 -->
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>
	<div class="container-fluid">
		<div class="row">
  			<div class="col-sm-2">
  				<div class="zTreeDemoBackground left">
  					<ul id="tree" class="ztree"></ul>
  				</div>
  			</div>
  			<div class="col-sm-7">
  				<div id="map" style="width: 100%; height: 500px"></div>
  				<button type="button" class="btn btn-primary" onclick="returnPrevious()">返回上级</button>
				<button id="save" type="button" class="btn btn-primary" onclick="saveDraw()">保存已绘制区域</button>
  			</div>
  			<div class="col-sm-3">
  				<textarea id="data_geo" style="width: 100%; height: 500px" disabled="disabled"></textarea>
  			</div>
		</div>
	</div>
</body>
<script type="text/javascript">
	var depthSetting = 2;
	var map = new BMap.Map('map');
	var parentDepth = depthSetting;
	var treeObj;
	var drawingManager;
	$(document).ready(function() {
		map.centerAndZoom(new BMap.Point(103.745, 29.600), 5);
		map.addControl(new BMap.NavigationControl());
		map.addControl(new BMap.ScaleControl());
		map.addControl(new BMap.OverviewMapControl());
		map.addControl(new BMap.MapTypeControl());
		map.enableScrollWheelZoom(true);
		
		init_tree();
		loadData(parentDepth);
	});
	

	function loadData(depth) {
		$("#save").hide();
		map.clearOverlays();
		
		if(drawingManager) {
			drawingManager._drawingTool.hide();
		}
		
		$.ajax({
			async : false,
			url : "${path}/map/getMapData",
			method : "post",
			data: {"depth": depth},
			beforeSend : function() {
				$("#body").showLoading();
			},
			success : function(data) {
				// 在地图点击时，选中树节点
				if(treeObj) {
					node = treeObj.getNodeByParam("id", depth);
					treeObj.selectNode(node);
				}
				
				if(data.length - 1==0) {
					// data数组中还包含了一个区域本身的范围，所以如果下级有1个区域，则data.length=1+1，如果下级没有区域，则data.length=1+0
					$.dialog.confirmByCallback('消息提示','所点击区域还没有划分下级区域，是否进入该区域？',
						function() {
							loadDataSelf(depth);
						},function() {
							loadData(parentDepth);
						}, 
					400);
				} else {
					parentDepth = depth;
					
					pointArray = [];

					count = data.length;
					data_geo_array = [];
					for (i = 0; i < count; i++) {
						pointList = data[i].pointList;
						if (pointList) {
							pointCount = pointList.length;
							points = [];
							
							for (j = 0; j < pointCount; j++) {
								point = pointList[j];
								points[j] = new BMap.Point(point.lon, point.lan);
							}
							
							if(data[i].is_root==1) {
								// 提供右侧文本框内容
								data_geo_array.push(data[i]);
								
								ply = new BMap.Polygon(points, {
									strokeWeight : 1,
									fillColor : data[i].fill_color
								});
								
								addMouseOverHandler(ply, data[i].area_name, data[i].area_desc);
								addClickHandler(ply, data[i].id);
								map.addOverlay(ply);
								pointArray = pointArray.concat(ply.getPath());
							} else if(data[i].is_root==2) {
								ply = new BMap.Polygon(points, {
									strokeWeight : 1,
									strokeStyle : "dashed"
								});
								
								map.addOverlay(ply);
								pointArray = pointArray.concat(ply.getPath());
							} else{}
						}
					}
					
					plyArray = [];
					for(i=0;i<data_geo_array.length;i++) {
						ply = data_geo_array[i];
						path_array = ply.pointList;
						pathArray = [];
						for(j=0;j<path_array.length;j++) {
							point = [];
							point.push(path_array[j].lon);
							point.push(path_array[j].lan);
							pathArray.push(point);
						}
						
						plyArray.push(pathArray);
					}
					
					$("#data_geo").val(JSON.stringify(plyArray, null, 2));

					map.setViewport(pointArray);
				}
				
				$("#body").hideLoading();
			},
			error : function() {
				alert("Data Error");
				$("#body").hideLoading();
			}
		});
	}
	
	function addClickHandler(ply, currentDepth) {
		ply.addEventListener("click", function(event) {
			loadData(currentDepth);
		});
	}
	
	function addMouseOverHandler(ply, title, info) {
		var opts = {
			width : 0, // 信息窗口宽度
			height : 0, // 信息窗口高度
			title : title, // 信息窗口标题
			enableMessage : true,//设置允许信息窗发送短息
			message : info
		}
		var infoWindow = new BMap.InfoWindow(info, opts); // 创建信息窗口对象 
		
		ply.addEventListener("mouseover", function(event) {
			map.openInfoWindow(infoWindow, event.point);
		});
	}
	
	function returnPrevious() {
		$.ajax({
			async : false,
			url : "${path}/map/getParentArea",
			method : "post",
			data :  {"currentArea":parentDepth},
			success : function(data) {
				if(data.p_id<depthSetting) {
					$.dialog.info("已经到达顶级区域");
				} else {
					loadData(data.p_id);
				}
			},
			error : function() {
				alert("Data Error");
			}
		});
	}
	
	function init_tree(obj){
		var setting = {
			edit: {
				enable: true,
				showRenameBtn: false
			},
			data: {
				simpleData: {
					enable: true,
					idKey: "id",
					pIdKey: "p_id"
				},
				key: {
					name: "area_name"
				}
			},
			callback: {
				onClick: function(event, treeId, treeNode) {
					loadData(treeNode.id);
				},
				onRemove: function(event, treeId, treeNode) {
					$.ajax({
						async : false,
						url : "${path}/map/deleteArea",
						method : "post",
						data :  {"id":treeNode.id},
						success : function(data) {
							if(data) {
								loadData(parentDepth);
							} else {
								alert("Data Error");
								init_tree();
								loadData(parentDepth);
							}
						},
						error : function() {
							alert("Data Error");
							init_tree();
							loadData(parentDepth);
						}
					});
				}
			}
		};
		$.ajax({
			async : false,
			url : "${path}/map/treeInvoke",
			method : "post",
			data :  {"rootArea":2},
			beforeSend : function() {

			},
			success : function(data) {
				if(data){
					treeObj = $.fn.zTree.init($("#tree"), setting, data);
					
					// 默认选中根节点
					var nodes = treeObj.getNodes();
					if (nodes.length>0) {
						treeObj.selectNode(nodes[0]);
					}
				}
			},
			error : function() {
				$.dialog.info("后台异常，未返回JSON数据");
			}
		});
	}
	
	function loadDataSelf(depth) {
		$("#save").show();
		$("#data_geo").val("");
		map.clearOverlays();
		
		$.ajax({
			async : false,
			url : "${path}/map/getMapDataSelf",
			method : "post",
			data: {"depth": depth},
			beforeSend : function() {
				$("#body").showLoading();
			},
			success : function(data) {
				parentDepth = depth;
				
				pointArray = [];

				if (data) {
					// 在地图点击时，选中树节点
					if(treeObj) {
						node = treeObj.getNodeByParam("id", depth);
						treeObj.selectNode(node);
					}
					
					pointCount = data.length;
					points = [];
					for (j = 0; j < pointCount; j++) {
						point = data[j];
						points[j] = new BMap.Point(point.lon, point.lan);
					}

					ply = new BMap.Polygon(points, {
						strokeWeight : 1,
						strokeStyle : "dashed"
					});
					
					map.addOverlay(ply);
					pointArray = pointArray.concat(ply.getPath());
					
					var overlaycomplete = function(e){
						e.overlay.enableEditing();
				    };
				    
				    // 如果工具条已存在就不再加入
				    if(!drawingManager) {
				    	var styleOptions = {
						        strokeColor:"red",    //边线颜色。
						        fillColor:"red",      //填充颜色。当参数为空时，圆形将没有填充效果。
						        strokeWeight: 1,       //边线的宽度，以像素为单位。
						        strokeOpacity: 0.8,	   //边线透明度，取值范围0 - 1。
						        fillOpacity: 0.6,      //填充的透明度，取值范围0 - 1。
						        strokeStyle: 'solid' //边线的样式，solid或dashed。
						    }
						    //实例化鼠标绘制工具
						    drawingManager = new BMapLib.DrawingManager(map, {
						        isOpen: false, //是否开启绘制模式
						        enableDrawingTool: true, //是否显示工具栏
						        drawingToolOptions: {
						            anchor: BMAP_ANCHOR_TOP_RIGHT, //位置
						            offset: new BMap.Size(5, 5), //偏离值
						            drawingModes : [
			                            BMAP_DRAWING_POLYGON
			                        ]
						        },
						        polygonOptions: styleOptions, //多边形的样式
						    });  
							 //添加鼠标绘制工具监听事件，用于获取绘制结果
						    drawingManager.addEventListener('overlaycomplete', overlaycomplete);
				    } else {
				    	drawingManager._drawingTool.show();
				    }
				}

				map.setViewport(pointArray);
				
				$("#body").hideLoading();
			},
			error : function() {
				alert("Data Error");
				$("#body").hideLoading();
			}
		});
	}
	
	function saveDraw() {
		ols = map.getOverlays();
		
		for(i=0;i<ols.length;i++) {
			if(ols[i].getPath) {
				// 如果进入表示该Overlay是多边形
				strokeStyle = ols[i].getStrokeStyle();
				if("solid"==strokeStyle) {
					// 如果进入表示该Overlay是用户新增的
					path = ols[i].getPath();
					str = "";
					for(j=0;j<path.length;j++) {
						if(j==path.length-1) {
							str = str + path[j].lng + ", " + path[j].lat;
						} else {
							str = str + path[j].lng + ", " + path[j].lat + ";"
						}
					}
					
					$.ajax({
						async : false,
						url : "${path}/map/saveDraw",
						method : "post",
						data :  {"pathStr":str, "p_id": parentDepth},
						success : function(data) {
							$.dialog.info("保存成功");
						},
						error : function() {
							$.dialog.info("保存失败");
						}
					});
				}
			}
		}
		
		init_tree();
		loadData(parentDepth);
	}
</script>
</html>