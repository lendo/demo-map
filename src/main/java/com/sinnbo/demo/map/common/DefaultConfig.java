package com.sinnbo.demo.map.common;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jfinal.render.ViewType;
import com.sinnbo.demo.map.action.BinAction;
import com.sinnbo.demo.map.action.MapAction;
import com.sinnbo.demo.map.domain.Area;
import com.sinnbo.demo.map.domain.AreaPathPoint;

public class DefaultConfig extends JFinalConfig {
	@Override
	public void configConstant(Constants constants) {
		loadPropertyFile("config.txt");
		constants.setDevMode(true);
		constants.setViewType(ViewType.JSP);
		constants.setBaseViewPath("/WEB-INF/jsp/");
	}

	@Override
	public void configHandler(Handlers handlers) {
		// TODO Auto-generated method stub

	}

	@Override
	public void configInterceptor(Interceptors interceptors) {
		// TODO Auto-generated method stub

	}

	@Override
	public void configPlugin(Plugins plugins) {
		C3p0Plugin c3p0Plugin = new C3p0Plugin(getProperty("jdbc.url"),getProperty("jdbc.user"), getProperty("jdbc.pwd").trim());
		plugins.add(c3p0Plugin);
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		plugins.add(arp);
		arp.addMapping("t_area", Area.class);
		arp.addMapping("t_area_path_point", AreaPathPoint.class);
	}

	@Override
	public void configRoute(Routes routes) {
		routes.add("/bin", BinAction.class);
		routes.add("/map", MapAction.class);
	}
	
	public static void main( String[] args ) {
        JFinal.start( "src/main/webapp", 80, "/", 1 );
    }
}
