package com.sinnbo.demo.map.domain;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.plugin.activerecord.Model;

public class Area extends Model<Area> {
	private static final long serialVersionUID = -9147245224509842822L;
	public final static Area dao = new Area();
	
	private List<AreaPathPoint> pointList = new ArrayList<AreaPathPoint>();

	public List<AreaPathPoint> getPointList() {
		return pointList;
	}

	public void setPointList(List<AreaPathPoint> pointList) {
		this.pointList = pointList;
	}
}
