package com.sinnbo.demo.map.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.sinnbo.demo.map.domain.Area;
import com.sinnbo.demo.map.domain.AreaPathPoint;

public class MapAction extends Controller {
//	public void index() {
//		Db.update("delete from t_area_path_point where area_id=21");
//		render("map.jsp");
//	}
	
	public void deleteArea() {
		Integer id = getParaToInt("id");
		List<Area> list = getSubAreaList(new ArrayList<Area>(), id);
		Area b = new Area();
		b.set("id", id);
		list.add(b);
		for(Area a : list) {
			Db.update("delete from t_area where id=?", a.getInt("id"));
			Db.update("delete from t_area_path_point where area_id=?", a.getInt("id"));
		}
		
		renderJson(1);
	}
	
	public void getPath() {
		String pathStr = getPara("pathStr");
		String[] pointArray = pathStr.split(";");
		for(String point : pointArray) {
			String[] s = point.split(", ");
			AreaPathPoint app = new AreaPathPoint();
			app.set("area_id", 21);
			app.set("lon", s[0]);
			app.set("lan", s[1]);
			app.save();
		}
	}
	
	public void showMap() {
		render("show-map.jsp");
	}
	
	public void getMapData() {
		Integer depth = getParaToInt("depth");
		List<Area> list = Area.dao.find("select * from t_area where p_id=?", depth);
		List<Map<String, Object>> outList = new ArrayList<Map<String, Object>>();
		
		List<AreaPathPoint> rootPath = AreaPathPoint.dao.find("select * from t_area_path_point where area_id=?", depth);
		Map<String, Object> rootPly = new HashMap<String, Object>();
		// 表示这些覆盖物是真实节点
		rootPly.put("is_root", 2);
		rootPly.put("pointList", rootPath);
		outList.add(rootPly);
		
		for(Area area : list) {
			List<AreaPathPoint> apps = AreaPathPoint.dao.find("select * from t_area_path_point where area_id=?", area.getInt("id"));
			Map<String, Object> a = new HashMap<String, Object>();
			a.put("id", area.getInt("id"));
			a.put("pointList", apps);
			a.put("area_name", area.getStr("area_name"));
			a.put("area_desc", area.getStr("area_desc")!=null ? area.getStr("area_desc") : "");
			a.put("zoom_min", area.getInt("zoom_min"));
			a.put("zoom_max", area.getInt("zoom_max"));
			a.put("fill_color", area.getStr("fill_color"));
			
			// 表示这些覆盖物是真实节点
			a.put("is_root", 1);
			outList.add(a);
		}
		
		renderJson(outList);
	}
	
	public void getMapDataSelf() {
		List<AreaPathPoint> apps = AreaPathPoint.dao.find("select * from t_area_path_point where area_id=?", getParaToInt("depth"));
		renderJson(apps);
	}
	
	public void getParentArea() {
		Integer currentArea = getParaToInt("currentArea");
		Area area = Area.dao.findById(currentArea);
		renderJson(area);
	}
	
	public void treeInvoke(){
		List<Area> areaList = getSubAreaList(null, getParaToInt("rootArea"));
        
        renderJson(areaList);
    }
	
	private List<Area> getSubAreaList(List<Area> finalList, Integer parentId) {
		if(finalList==null) {
			finalList = new ArrayList<Area>();
			
			Area rootArea = Area.dao.findById(parentId);
			finalList.add(rootArea);
		}
		
		List<Area> localList = Area.dao.find( "select * from t_area where p_id=?", parentId);
		
		if(localList!=null && !localList.isEmpty()) {
			finalList.addAll(localList);
			
			for(Area area : localList) {
			    getSubAreaList(finalList, area.getInt("id"));
			}
		}
		
		return finalList;
	}
	
	public void saveDraw() {
		Area area = new Area();
		Integer parentId = getParaToInt("p_id");
		
		area.set("p_id", parentId);
		area.set("area_name", "区域" + UUID.randomUUID().toString().substring(0, 8));
		
		area.save();
		
		String pathStr = getPara("pathStr");
		String[] pointArray = pathStr.split(";");
		for(String point : pointArray) {
			String[] s = point.split(", ");
			AreaPathPoint app = new AreaPathPoint();
			app.set("area_id", area.getInt("id"));
			app.set("lon", s[0]);
			app.set("lan", s[1]);
			app.save();
		}
		
		renderJson(1);
	}
}
