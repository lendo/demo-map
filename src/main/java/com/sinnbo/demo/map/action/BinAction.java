package com.sinnbo.demo.map.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jfinal.core.Controller;

public class BinAction extends Controller {
	public void index() {
		render("bin.jsp");;
	}
	
	public void indexData() {
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		for(int i=0;i<10;i++) {
			Map<String, String> m = new HashMap<String, String>();
			m.put("name", "中国" + i);
			m.put("code", "ZH-CN" + i);
			list.add(m);
		}
		
		renderJson("data", list);
	}
}