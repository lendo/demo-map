# README #

百度地图勾线工具For罗建雄

### What is this repository for? ###

* 用于演示百度地图JavaScript API的相关用法
* 用于在线勾勒某一区域，即时生成区域Path路线的JSON格式数据
* [百度地图示例](http://lbsyun.baidu.com/index.php?title=jspopular)
* [百度地图API](http://developer.baidu.com/map/reference/)

### How do I get set up? ###

* 工程为Maven项目，需要安装JDK，Eclipse和Maven
* 数据库为MySQL
* 数据库脚本文件：/demo-map/sql/demo-map.sql
* 项目数据库连接配置文件：/demo-map/src/main/resources/config.txt
* 配置完成后，在项目根目录运行：mvn jetty:run
* 访问地址：http://localhost:8080/map/showMap

### Contributors ###

* @lendo 